;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;DECLARATION;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Tentative naming conventions
;;;;;;;;;;;;;;;;;;;;;;;;;;;
; for reporter-like variables :
; infected : in state I or IA
; contagious : in state E, I or IA
; nb-var or var : number of var at current tick
; daily-var : number of var for the whole day
; total-var : sum/cumulation of vars since the beginning of the simulation
; var% : var / population size
; proportion-var : var / something

globals [ ;;global parameters
  ;epidemic symbolic constants
  S ; Susceptible
  Ex ; Exposed - Infected and incubating, but already contagious.
  Ia ; Infected asymptomatic
  I ; Infected symptomatic
  R ; Recovered

  delay-before-test
  incubation-duration
  nb-days-before-test-tagging-contacts
  proportion-equiped
  probability-respect-lockdown
  probability-success-test-infected
  probability-asymptomatic-infection
  R0-a-priori
  initial-R-proportion
  nb-cases-at-initialisation
  size_population
  Nb_contagious_initialisation
  quarantine-time

  population-size
  nb-house
  probability-transmission
  probability-transmission-asymptomatic
  walking-angle
  speed
  probability-car-travel
  ;current-nb-new-infections-reported
  ;current-nb-new-infections-asymptomatic
  transparency
  infection-duration
  contagion-duration-tick
  nb-ticks-per-day
  lockdown-date
  previous-lockdown-state
  lock-down-color
  nb-lockdown-episodes
  max-I
  max-conf
  max-nb-lockdown

  nb-contagious-detected
  total-nb-contagious-lockeddown
  total-nb-contagious
  total-nb-non-contagious-lockeddown
  total-nb-lockeddown
  total-lockeddown-tracked
  nb-co-infected
  mean-daily-contacts ; for 1 tick
  mean-mean-daily-contacts ; mean since the beginning of the simulation
  Estimated-mean-mean-daily-contacts
  contacts-to-warn
  contacts-to-warn-next
  list-mean-contacts

  total-contagious-lockeddown-symptom
  total-contagious-lockeddown-tracked

  tracers-this-tick
  traced-this-tick
  REACTING? ; doing anything?
  TRACING? ; contact-TRACING?
  TESTING? ; secondary testing, primary infected is always tested
  FAMILY-LOCKDOWN?
  fixed-seed?
]

patches-own [wall]

breed [citizens citizen]
breed [houses house]

citizens-own
[
  epidemic-state; S, Ex, Ia, I or R
  infection-date
  infection-source
  nb-other-infected
  contagion-counter ;;counter to go from state 1 or 2 (infected) to state 3 recovered
  contagious? ; boolean
  my-contagiousness
  resistant? ; boolean - indicates in case of infection wether the citizen will express symptoms
  my-house
  lockdown? ; 0 free 1 locked
  nb-ticks-lockdown
  liste-contacts
  liste-contact-dates
  daily-contacts
  equiped?
  detected?
  list-date-test
  nb-tests
  nb-lockdown
  contact-order ;0 not contacted, 1 contacted at first order, 2 contacted at second order
  nb-contacts-ticks
  nb-contacts-total-Infectious ; total number of contacts during infectious period
  difference
  potential-co-infected
  family-infection?
  delayed-test
  to-be-tested
]

houses-own
[
  my-humans
  clean ; private. Should be only accessed through reporter clean? [house]
  unlock-time ; private. Should only be accessed through reporter unlock-time? [house]
]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;SETUP;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup-globals
  ;symbolic constants
  set S 0
  set Ex 1
  set Ia 2
  set I 3
  set R 4

  set delay-before-test  Waiting-Time-Before-Testing
  set nb-days-before-test-tagging-contacts Depth-for-contact-identification
  set proportion-equiped Coverage-Rate-of-Tracing-App
  set probability-respect-lockdown Probability-of-compliance-isolation
  set probability-success-test-infected Probability-that-the-test-is-effective
  set R0-a-priori R0-set
  set initial-R-proportion 0
  set nb-cases-at-initialisation 10
  set size_population 2000
  set Nb_contagious_initialisation nb-cases-at-initialisation

  set fixed-seed? false
  if fixed-seed?[
    random-seed 30
  ]

  set population-size  size_population
  set nb-house (population-size / 3)

  set walking-angle 50
  set speed 0.5
  set probability-car-travel 0.2
  set transparency 145
  set nb-ticks-per-day 4
  set incubation-duration 4
  set infection-duration 14
  set quarantine-time (infection-duration + incubation-duration) * nb-ticks-per-day ;why not?
  set contagion-duration-tick ((incubation-duration + infection-duration) * nb-ticks-per-day) ;
  set probability-asymptomatic-infection 0.3

  set Estimated-mean-mean-daily-contacts 0.004 * population-size + 3.462 ;calibrated from systematic experiments from 1000 to 10000 agents, on same world
  set probability-transmission R0-a-priori / ((Estimated-mean-mean-daily-contacts / nb-ticks-per-day)  * contagion-duration-tick) ; probability per tick
  set probability-transmission-asymptomatic probability-transmission / 2

  set contacts-to-warn-next no-turtles
  set contacts-to-warn no-turtles
  set list-mean-contacts []
  set mean-mean-daily-contacts []






  ifelse SCENARIO = "Let it go"[
    set REACTING? false
    set TRACING? false
    set TESTING? false
    set FAMILY-LOCKDOWN? false
  ][ifelse SCENARIO = "Simple isolation"[
    set REACTING? true
    set TRACING? false
    set TESTING? false
    set FAMILY-LOCKDOWN? true
  ][ifelse SCENARIO = "Tracing and systematic isolation"[
    set REACTING? true
    set TRACING? true
    set TESTING? false
    set FAMILY-LOCKDOWN? false
  ][ifelse SCENARIO = "Tracing and selective isolation"[
    set REACTING? true
    set TRACING? true
    set TESTING? true
    set FAMILY-LOCKDOWN? false
  ][
    show "error"
    stop
]]]]


end

to setup-walls
  ask patches with [abs(pxcor) = max-pxcor or abs(pycor) = max-pycor] [set wall 1]
end


to setup-houses
  create-houses nb-house[
    move-to one-of patches with [wall = 0]
    set shape "house"
    fd random-float 0.5
    setxy random-xcor random-ycor
    set size 2
    set color lput transparency extract-rgb  white
    set my-humans []
    set clean true
  ]
end

to setup-population
  create-citizens population-size
  [
    let me self
    move-to one-of patches with [wall = 0]
    fd random-float 0.5
    set shape "circle"
    set size 1
    set color green ; lput transparency extract-rgb  green
    set epidemic-state S
    set delayed-test delay-before-test / 6
    set to-be-tested false
    set nb-contacts-ticks 0
    set my-house one-of houses
    ask my-house[
      set my-humans lput me my-humans
    ]
    set liste-contacts []
    set liste-contact-dates []
    set list-date-test []
    set daily-contacts nobody
    set nb-other-infected 0
    set contact-order 0
    set to-be-tested false
    set potential-co-infected false
    set family-infection? false
    set equiped? false
    set detected? false
    set contagious? false
    set resistant? false ; resistance is really "defined" when the citizen is Exposed to the virus
  ]
  set-infected-initialisation
  set-R-initialisation
  set-equiped-initialisation
end

to set-equiped-initialisation
  ask n-of (round (population-size * (Proportion-equiped / 100))) citizens[
    set equiped? true
  ]
end


to set-infected-initialisation
  ask n-of Nb_contagious_initialisation citizens [
    become-exposed
  ]
end

to set-R-initialisation
  ask n-of (initial-R-proportion / 100 * population-size) citizens with [not (epidemic-state = Ex)][
    set epidemic-state R
  ]
end


to setup
  clear-all
  reset-ticks
  setup-globals
  setup-walls
  setup-houses
  setup-population
end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;GO;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to go
  if not any? citizens with [contagious?] [stop]

  move-citizens
  get-in-contact

  if TRACING?[
;    set tracers-this-tick 0
;    set traced-this-tick 0
    if any? contacts-to-warn[
      warn-contacts 2
    ]
  ]
  if REACTING?[
    ask citizens with [(epidemic-state = I) and (not detected?) and (lockdown? = 0) and (to-be-tested = false)] [
      set to-be-tested true
      set contact-order 1
    ]
    ask citizens with [to-be-tested = true][
      ifelse delayed-test = 0[
        get-tested
      ][
        set delayed-test delayed-test - 1
      ]
    ]
  ]

  if TRACING?[
    set traced-this-tick count contacts-to-warn-next
    set contacts-to-warn contacts-to-warn-next
    set contacts-to-warn-next no-turtles
  ]


  update-epidemics
  update-lockdown

  update-max-I
  update-max-conf
  update-list-mean-contacts
  update-mean-daily-contacts
  update-mean-mean-daily-contacts
  update-my-contagiousness

  tick

end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;MOVEMENT PROCEDURES;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to move-citizens
  ask citizens[
    ifelse lockdown? = 1[


    ][
      let nighttime? (ticks mod nb-ticks-per-day) = 0
      ifelse nighttime?[
        move-to my-house
      ][
        ifelse random-float 1 < probability-car-travel[
          move-to one-of patches with [wall = 0]
        ][
          rt random 360
          avoid-walls
          fd speed
        ]
      ]
    ]
  ]
end

to avoid-walls
  ifelse  any? neighbors with [wall = 1]
    [ face one-of neighbors with [wall = 0] ]
  [set heading heading + random walking-angle - random walking-angle
      ]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;EPIDEMIC PROCEDURES;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to get-virus [contact-source]
  ifelse ( ([contagious?] of contact-source)  and (random-float 1 < (contagiousness contact-source)) ) [
    become-exposed
    if lockdown? = 1[
      set total-nb-contagious-lockeddown total-nb-contagious-lockeddown + 1
    ]
    set infection-source contact-source
    ask contact-source [set nb-other-infected nb-other-infected + 1]
    set family-infection? (my-house = [my-house] of contact-source)
    if potential-co-infected [
      set nb-co-infected nb-co-infected + 1
    ]
  ][
    set potential-co-infected false
  ]
end

to update-epidemics
  ;;update the counters for the infected at this timestep
  ;set current-nb-new-infections-reported 0
  ;set  current-nb-new-infections-asymptomatic 0
  ;;update recovered
  ask citizens with [contagious?][
    set contagion-counter (contagion-counter - 1)
    if ( (ticks - infection-date) = (incubation-duration * nb-ticks-per-day)) [
      ifelse resistant?
        [ become-asymptomatic-infected ]
        [ become-infected ]
    ]

    if contagion-counter <= 0 [
      become-recovered
    ]
  ]
end


to get-in-contact
  ask citizens
  [
    ifelse lockdown? = 0[
;      if (((ticks - 1) mod nb-ticks-per-day) = 0)[
;        set daily-contacts nobody
;      ]
      let contacts (turtle-set other citizens-here) ; citizens-on neighbors
;      set nb-contacts-ticks count contacts
      set daily-contacts (turtle-set daily-contacts contacts)

;      if contagious? [set nb-contacts-total-Infectious nb-contacts-total-Infectious + nb-contacts-ticks]

      let family []
      ask my-house[
        set family my-humans
      ]
      let family-contacts contacts with [member? self family]
      set contacts contacts with [lockdown? = 0]
      set contacts (turtle-set contacts family-contacts)

      if equiped? [
        let contacts-equiped contacts with [equiped?]
        set liste-contacts lput contacts-equiped liste-contacts
        set liste-contact-dates lput ticks liste-contact-dates
      ]

      let infection-contact one-of contacts with [contagious?]
      if epidemic-state = S and is-agent? infection-contact [
        get-virus infection-contact
      ]
    ][
      if epidemic-state = S[
        let co-infected? false
        let infection-contact nobody
        ask my-house[
          foreach my-humans[
            [my-human] -> ask my-human [
              if contagious?[
                set co-infected? true
                set infection-contact self
              ]
            ]
          ]
        ]
        if co-infected?[
          get-virus infection-contact
          set potential-co-infected true
        ]
      ]
    ]
  ]
end

to get-tested
  set list-date-test lput ticks list-date-test
  set nb-tests nb-tests + 1
  ;set delayed-test delay-before-test / 6
  set to-be-tested false
  ;test results and consequences
  if contagious? and random-float 1 < probability-success-test-infected [
    set detected? true
    set  nb-contagious-detected nb-contagious-detected + 1
    if (random-float 1 < probability-respect-lockdown)[
      ifelse FAMILY-LOCKDOWN? [
        ask my-house[
          foreach my-humans[
            [my-human] -> ask my-human [
              if lockdown? = 0 [
                lockdown
              ]
            ]
          ]
        ]
      ][
        if lockdown? = 0 [
          lockdown
        ]
      ]
    ]
    if equiped? and TRACING?[
        detect-contacts
      ]
  ]

end

to lockdown
  set lockdown? 1
  set nb-lockdown nb-lockdown + 1 ;
  if nb-lockdown > max-nb-lockdown[
    set max-nb-lockdown nb-lockdown
  ]
  set lockdown-date ticks
  move-to my-house
  set nb-ticks-lockdown quarantine-time
  if nb-lockdown = 1 [
    set total-nb-lockeddown total-nb-lockeddown + 1 ; le nombre de personnes ayant été confinées au moins une fois
  ]
   ;les compteurs ci-dessous sont incrémentés à chaque fois que la personne est confinée
    ifelse contagious?[
      set total-nb-contagious-lockeddown total-nb-contagious-lockeddown + 1
      ifelse contact-order = 1 [
        set total-contagious-lockeddown-symptom total-contagious-lockeddown-symptom + 1
      ][
        set total-contagious-lockeddown-tracked total-contagious-lockeddown-tracked + 1
      ]
    ][
      set total-nb-non-contagious-lockeddown total-nb-non-contagious-lockeddown + 1
    ]
  if contact-order = 2[
    set total-lockeddown-tracked total-lockeddown-tracked + 1
  ]
end

to update-lockdown
  ask citizens with [lockdown? = 1][
    set nb-ticks-lockdown (nb-ticks-lockdown - 1)
    if ((clean? my-house) and (unlock-time? my-house))[
        ask my-house[
        foreach my-humans[
          [my-human] -> ask my-human [
            set lockdown? 0
          ]
        ]
      ]
      ;show ("Freedom")
    ]
  ]
end



;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;BACKTRACKING;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;


to detect-contacts
  let me self
  let my-lockdown-date lockdown-date
  let j 0
  set tracers-this-tick tracers-this-tick + 1
  repeat length liste-contacts[

    let date-j item j liste-contact-dates
    let contacts-j item j liste-contacts

    if is-agentset? contacts-j[
      if date-j >= (my-lockdown-date - (nb-days-before-test-tagging-contacts * nb-ticks-per-day))[
        set contacts-to-warn-next (turtle-set contacts-to-warn-next contacts-j) ; with [detected? = false]
      ]
    ]
    set j j + 1
  ]
  set liste-contacts []
  set liste-contact-dates []
end

to warn-contacts [order]
  ifelse TESTING?[
    ask contacts-to-warn with [detected? = false][
      set to-be-tested true
      set contact-order order
    ]
  ][
    ask contacts-to-warn [
      ifelse (lockdown? = 0)[
        if random-float 1 < probability-respect-lockdown[
          set contact-order order
          lockdown
        ]
      ][
        set nb-ticks-lockdown quarantine-time
     ]
     detect-contacts
    ]
  ]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;STATE TRANSITION PROCEDURES;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to become-exposed
  set epidemic-state Ex
  set contagion-counter contagion-duration-tick
  set infection-date ticks
  ;set current-nb-new-infections-reported (current-nb-new-infections-reported + 1)
  set total-nb-contagious total-nb-contagious + 1
  set color brown ;
  set resistant? (random-float 1 < probability-asymptomatic-infection)
  set contagious? true
  set my-contagiousness contagiousness self
end

to become-infected
  set epidemic-state I
  set color red ;
set my-contagiousness contagiousness self
end

to become-asymptomatic-infected
  set epidemic-state Ia
  set color blue ;
set my-contagiousness contagiousness self
end

to become-recovered
  set epidemic-state R
  set contagious? false
  set color yellow ;
  set my-contagiousness 0
end



;###############################
;REPORTERS
;###############################

to-report contagiousness [a-citizen]
  if ([epidemic-state] of a-citizen) = Ex [
    ifelse resistant? [
      report (((ticks - infection-date) / (incubation-duration * nb-ticks-per-day)) * probability-transmission-asymptomatic) ; linear growth from 0 at infection time to full asymptomatic transmission probability at the end of incubation time
    ][
      report (((ticks - infection-date) / (incubation-duration * nb-ticks-per-day)) * probability-transmission) ; linear growth from 0 at infection time to full symptomatic transmission probability at the end of incubation time
    ]
  ]

  if ([epidemic-state] of a-citizen) = I [
      report probability-transmission
  ]

  if ([epidemic-state] of a-citizen) = Ia [
      report probability-transmission-asymptomatic
  ]

end

to-report clean? [a-house]
  let local-clean true
  ask a-house[
    foreach my-humans[
      [my-human] -> ask my-human [
        if contagious?[
          set local-clean false
        ]
      ]
    ]
    set clean local-clean
  ]
  report [clean] of a-house
end

to-report unlock-time? [a-house]
  let local-unlock-time true
  ask a-house[
    foreach my-humans[
      [my-human] -> ask my-human [
        if (nb-ticks-lockdown > 0)[
          set local-unlock-time false
        ]
      ]
    ]
    set unlock-time local-unlock-time
  ]
  report [unlock-time] of a-house
end

to-report nb-S
  report count citizens with [epidemic-state = S ]
end

to-report nb-Ir
  report count citizens with [epidemic-state = I ]
end

to-report nb-Ex
  report count citizens with [epidemic-state = Ex ]
end

to-report nb-Inr
  report count citizens with [epidemic-state = Ia ]
end

to-report nb-I
  report count citizens with [epidemic-state = I or epidemic-state = Ia ]
end

  to-report nb-R
  report count citizens with [epidemic-state = R]
end

to-report nb-non-S%
  report (population-size - nb-S) / population-size * 100
end


to-report nb-S%
report (nb-S) / population-size * 100
end

to-report epidemic-duration-final
  report round (ticks / nb-ticks-per-day)
end


to-report lockdowned%
  report (count citizens with [lockdown? = 1] / population-size) * 100
end

to-report total-nb-tests
  report  sum [nb-tests] of citizens
end

to-report population-tested
  report  count citizens with [nb-tests > 0]
end

to-report population-tested%
  report  count citizens with [nb-tests > 0] / population-size * 100
end

to-report total-population-locked%
  report total-nb-lockeddown / population-size * 100

end

to-report proportion-non-contagious-lockeddown%
  ifelse (total-nb-contagious-lockeddown + total-nb-non-contagious-lockeddown) > 0
  [report total-nb-non-contagious-lockeddown / (total-nb-contagious-lockeddown + total-nb-non-contagious-lockeddown) * 100]
  [report 0]

end

to-report nb-detected
  report count citizens with [detected?]
end

to-report nb-detected%
  report nb-detected / population-size * 100
end

to-report contagious-detected%
  ifelse total-nb-contagious > 0
  [report nb-contagious-detected / total-nb-contagious * 100]
  [report 0]
end

to-report contagious-lockeddown%
  ifelse total-nb-contagious > 0
  [report total-nb-contagious-lockeddown / total-nb-contagious * 100]
  [report 0]
end

to-report %detected
  report (count citizens with  [detected?] / population-size) * 100
end

to-report epidemic-duration
  report round (ticks / nb-ticks-per-day)

end

to-report MaxI%
 report max-I / population-size * 100
end

to-report Max-Conf%
  report max-conf /  population-size  * 100
end

to-report R0
  ;report mean [nb-other-infected] of citizens with [ epidemic-state != S] - To see in real time the nb of other citizens infected by people who wera affected by the infection
  ifelse nb-R > 0 [
    report mean [nb-other-infected] of citizens with [epidemic-state = R]
  ][
    report "N/A"
  ]
  ;To see once someone is cured in average how many people he infected. Converges towards 1 as time goes by as the population is finite:
  ;the sum of nb-other-infected is the population of infected, as in our model an infected has one and only one source
end

to-report family-locked-down
  report count citizens with [not detected? and lockdown? = 1]
end

to-report mean-contacts-ticks
  report mean [nb-contacts-ticks] of citizens
end

to-report symptom-detected
  report count citizens with [detected? and contact-order = 1]
end

to-report symptom-detected%
  ifelse nb-detected > 0
  [report symptom-detected /  nb-detected  * 100]
  [report 0]
end

to-report contact-detected
  report count citizens with [detected? and contact-order = 2]
end

to-report contact-detected%
  ifelse nb-detected > 0
  [report contact-detected /  nb-detected  * 100]
  [report 0]
end

to-report contagious-lockeddown-tracked
  report count citizens with [(lockdown? = 1) and (contagious?) and (contact-order = 2)]
end

to-report proportion-total-contagious-lockeddown-tracked
  ifelse (total-contagious-lockeddown-tracked + total-contagious-lockeddown-symptom) > 0[
    report total-contagious-lockeddown-tracked / (total-contagious-lockeddown-tracked + total-contagious-lockeddown-symptom) * 100
  ][
    report 0
  ]
end

to-report proportion-total-contagious-lockeddown-symptom
  ifelse (total-contagious-lockeddown-tracked + total-contagious-lockeddown-symptom) > 0[
    report total-contagious-lockeddown-symptom / (total-contagious-lockeddown-tracked + total-contagious-lockeddown-symptom) * 100
  ][
    report 0
  ]
end

to-report citizens-per-house
  report mean [length my-humans] of houses
end

to-report mean-contacts
  report mean list-mean-contacts
end

to-report mean-mean-daily-contacts-nb
  report mean mean-mean-daily-contacts
end





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;COUNTERS;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


to update-max-I
  if nb-I > max-I [set max-I nb-I]
end

to update-max-conf
  let nb-conf count citizens with [lockdown? = 1]
  if nb-conf > max-conf [set max-conf nb-conf]
end

to update-list-mean-contacts
  set list-mean-contacts lput mean-contacts-ticks list-mean-contacts
end

to update-mean-daily-contacts
  if ((ticks mod 4) = 0)[
    set mean-daily-contacts mean [count daily-contacts] of citizens
  ]
end

to update-mean-mean-daily-contacts
  set mean-mean-daily-contacts lput mean-daily-contacts mean-mean-daily-contacts
end

;we update contagiousness only for Exposed citizens, as the value grows as ticks pass
to update-my-contagiousness
  ask citizens with [epidemic-state = 1 ]
  [set my-contagiousness contagiousness self]

end
@#$#@#$#@
GRAPHICS-WINDOW
8
6
626
825
-1
-1
10.0
1
10
1
1
1
0
0
0
1
-30
30
-40
40
1
1
1
ticks
30.0

BUTTON
630
828
725
870
Setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
727
828
818
871
Run Model
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
628
6
1401
265
EPIDEMIC DYNAMIC
Epidemic duration
% population
0.0
10.0
0.0
100.0
true
true
"" ""
PENS
"Susceptible" 1.0 0 -13840069 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day) (nb-S / population-size * 100)]"
"Exposed" 1.0 0 -6459832 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day) (nb-Ex / population-size  * 100)]"
"I. Symptomatic" 1.0 0 -2139308 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day) (nb-Ir  / population-size * 100)]"
"I. Asymptomatic" 1.0 0 -1184463 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day) (nb-Inr  / population-size * 100)]"
"Contagious" 1.0 0 -955883 true "" "\nif population-size > 0 [plotxy (ticks / nb-ticks-per-day) ((nb-Ir + nb-Inr + nb-Ex) / population-size  * 100)]"
"Removed" 1.0 0 -13791810 true "" "\nif population-size > 0 [plotxy (ticks / nb-ticks-per-day) (nb-R / population-size  * 100)]"
"Isolated" 1.0 0 -8630108 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day) lockdowned%] "

MONITOR
897
362
1120
407
Population hit by epidemy (%)
nb-non-S%
1
1
11

CHOOSER
394
828
627
873
SCENARIO
SCENARIO
"Let it go" "Simple isolation" "Tracing and systematic isolation" "Tracing and selective isolation"
3

MONITOR
1123
362
1402
407
Epidemic duration (in days)
epidemic-duration-final
0
1
11

MONITOR
628
361
895
406
Epidemic peak (%)
MaxI%
1
1
11

SLIDER
9
934
331
967
Probability-that-the-test-is-effective
Probability-that-the-test-is-effective
0
1
0.95
0.1
1
NIL
HORIZONTAL

SLIDER
332
934
630
967
Probability-of-compliance-isolation
Probability-of-compliance-isolation
0
1
1.0
0.1
1
NIL
HORIZONTAL

SLIDER
9
897
387
930
Depth-for-contact-identification
Depth-for-contact-identification
1
5
5.0
1
1
days
HORIZONTAL

SLIDER
9
827
388
860
Coverage-rate-of-tracing-app
Coverage-rate-of-tracing-app
0
100
100.0
10
1
%
HORIZONTAL

SLIDER
9
862
388
895
Waiting-time-before-testing
Waiting-time-before-testing
0
72
0.0
6
1
hours
HORIZONTAL

MONITOR
1265
409
1403
454
Population tested (%)
population-tested%
1
1
11

MONITOR
784
410
938
455
Population isolated (%)
total-population-locked%
1
1
11

MONITOR
1122
266
1401
311
Infected
nb-I
0
1
11

MONITOR
1123
315
1402
360
Removed
nb-R
0
1
11

MONITOR
897
266
1120
311
Exposed
nb-Ex
0
1
11

MONITOR
628
266
894
311
Susceptible
nb-S
0
1
11

MONITOR
1151
733
1404
778
Contagious population isolated (%)
contagious-lockeddown%
1
1
11

MONITOR
893
733
1149
778
Contagious population identified (%)
contagious-detected%
1
1
11

PLOT
629
458
1403
733
EFFICACY OF MEASURES TAKEN
Epidemic duration
Cumulated number
0.0
10.0
0.0
10.0
true
true
"" ""
PENS
"Contagious (total)" 1.0 0 -817084 true "" "\nif population-size > 0 [plotxy (ticks / nb-ticks-per-day) total-nb-contagious ]"
"Contagious identified" 1.0 0 -2674135 true "" "\nif population-size > 0 [plotxy (ticks / nb-ticks-per-day) nb-contagious-detected]\n\n"
"Contagious isolated" 1.0 0 -5825686 true "" "\nif population-size > 0 [plotxy (ticks / nb-ticks-per-day) total-nb-contagious-lockeddown]\n\n"
"Susceptible or removed isolated" 1.0 0 -13840069 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day) total-nb-non-contagious-lockeddown]"
"Population tested" 1.0 0 -12895429 true "" "if population-size > 0 [plotxy (ticks / nb-ticks-per-day)  population-tested]"

MONITOR
630
734
894
779
Total contagious population
total-nb-contagious
0
1
11

MONITOR
628
409
782
454
Isolation peak (%)
Max-Conf%
1
1
11

SLIDER
446
886
581
919
R0-set
R0-set
1
5
2.0
1
1
NIL
HORIZONTAL

MONITOR
628
314
894
359
Symptomatic cases
nb-Ir
0
1
11

MONITOR
897
314
1121
359
Asymptomatic cases
nb-Inr
0
1
11

TEXTBOX
869
829
1399
1021
How to\n----------\n\nChoose a scenario and a combination of initial conditions with bottom sliders, before clicking the \"Setup\" and then the \"Run Model\" button. Simulation speed can be modulated with top slider (\"normal speed\" by default).\n\nNB: duration of contagious period is 14 days, plus 4 incubation days. \n\n
14
55.0
1

MONITOR
631
874
851
919
Total population
Population-size
0
1
11

MONITOR
632
923
851
968
% Population infected at initialisation
nb-cases-at-initialisation / Population-size * 100
2
1
11

MONITOR
630
780
998
825
Contagious population isolated by tracing (%)
proportion-total-contagious-lockeddown-tracked
1
1
11

MONITOR
998
780
1404
825
Contagious population isolated by symptoms (%)
proportion-total-contagious-lockeddown-symptom
1
1
11

MONITOR
939
409
1262
454
Non-contagious population isolated (%) 
proportion-non-contagious-lockeddown%
2
1
11

@#$#@#$#@
## DESCRIPTION

This model is a combination of three different sub-models (mobility, epidemiologic and contact tracing), allowing explorating by simulation the impact of contact tracing application on COVID-19 dynamic, in a simplified artificial society.

Mobility component

This is an agent-based model of a simplified environment populated with a limited number of mobile individuals. The days are divided into four 6-hour segments. The first three sections are occupied by daily mobility and close social interactions(on average 10 per day and per person).
Most trips are made near home, but 20% of them are longer distances. The last part of the day is spent at home, which is shared by an average of three people. In total, 2000 people occupy this small virtual territory.

On top this mobility model lies a SEIR epidemic model with two possible categories of Infection: symptomatic and asymptomatic, the latter being more difficult to identify by simple medical diagnosis due to the absence of symptoms.

Epidemiological component

At initialization, all individuals are healthy (Susceptible). A small number of Infected individuals is then injected into this initial population and the spread of the virus is simulated from the behaviors of the individuals modeled.
Changes of state take place as follows and use the reference values​set by the Institut Pasteur:

* Susceptible → Exposed: a Susceptible individual will become Exposed, in contact with an Infected individual, according to a probability that depends on the value of R0 chosen and the category to which this infected individual belongs (see next point). The formula used to calculate this probability is as follows: P (S → E) = 1 / R0 x c x d, with c the number of average contacts per day (fixed at around 10) and d the duration of the contagious period.

* Exposed → Infected: an individual will remain in the Exposed state during his incubation period (fixed here at 5 days), during which he will gradually become contagious, until becoming Infected asymptomatic (3 chances out of 10) or Infected symptomatic (7 chances out of 10). The contagiousness of a symptomatic individual is estimated from the R0 (see previous point) and is considered to be twice that of an asymptomatic individual.

* Infected → Removed: after 14 days, an Infected individual is considered Removed and is not contagious anymore in the model.

Contract tracing component 

At every time step, an agent A equiped with the contact tracing app records all the agents also equipped in its immediate surroundings, along with the date of the contact. If A is later tested positive, and if it accepts to take measures accordingly (as described in Scenarios below), it will notify all the agents in its recorded contacts list down to a parametrable date -- 5 days ago in the scenarios below. At the next time step, depending on the scenarios, the notified agents will either start directly the countermeasures such as lockdown, or get tested first. They then find themselves in the same position as the agent A. The contact network is therefore explored width-first, at a speed of all the nodes at a distance of 1 at each time step. 

Scenarios

On this basis, four distinct scenarios are proposed, in a comparative perspective. 

* S1 “Let it go”: we do nothing, the epidemic is going on without any interference;     
     
* S2 “Simple isolation”: symptomatic carriers are identified (tested) and isolated with their families (understood here as people who share the same roof);     
     
* S3 “Tracing and systematic isolation”: symptomatic carriers are systematically tested and those who are positive are isolated while their contacts are isolated without being tested;     
     
* S4 “Tracing and selective isolation”: symptomatic carriers are systematically tested and those who are positive are isolated, while their contacts (and their contacts) are tested and then isolated if the test is positive.     



## HOW TO USE IT

Choose a scenario and a combination of initial conditions with bottom sliders, before clicking the "Setup" and then the "Run Model" button.


## PRECISIONS ON R0

The R0, or basic reproduction number, is a global indicator of epidemic dynamics. Intuitively it can be seen as an indicator of how many healthy people a contagious person will infect, on average, at the start of the epidemic. During the epidemic, the number of reproductions will change, we will speak of the temporal  reproduction number or R (t). If R0, which is therefore also R (0), represents the intrinsic "ferocity" of the epidemic in a given territory, R (t) will represent the evolution of contagion over time and according to the measures applied. If the value of R over time drops below 1, the epidemic will stop on its own. If R remains above 1, then the epidemic will continue to grow.
R0 is a composition of 3 coupled processes: the rate of contacts in the population, the probability of transmission of the virus at each contact and the duration of the contagious phase of the disease developed. Moreover, like any global indicator, it does not take into account local situations, which can be very different from one to another. It must therefore be handled with care. Its use in this model makes it possible to characterize typical epidemic dynamics. For example, the R (t) in France at the time of the implementation of the first period of confinement (March 17, 2020) was estimated at a value between 2.5 and 3.5. After several weeks of confinement, R (t) had fallen to a value of less than 1, thanks to the implementation of confinement but also to the adoption of protective measures including physical distancing applied in a relatively generalized manner. In this sense, the underlying epidemic dynamics observed at that time can be considered artificial and therefore no longer reflects the initial dynamics, as modeled here.



## AUTEURS

Model developped par Arnaud Banos & Pierrick Tranouez for COVprehension (https://covprehension.org/)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

circle white
false
0
Circle -7500403 true true 0 0 300
Circle -1 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

sploch
true
0
Polygon -2674135 true false 60 105 45 60 90 75 120 15 150 90 240 45 240 90 285 165 210 225 210 165 180 195 165 255 135 240 135 180 45 225 120 120 30 135

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="Explo_V9_Scenarios3-4" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <metric>Max-Conf%</metric>
    <metric>total-population-locked%</metric>
    <metric>population-tested%</metric>
    <metric>total-nb-tests</metric>
    <metric>total-nb-contagious</metric>
    <metric>contagious-detected%</metric>
    <metric>contagious-lockeddown%</metric>
    <metric>proportion-non-contagious-lockeddown%</metric>
    <metric>proportion-total-contagious-lockeddown-tracked</metric>
    <metric>proportion-total-contagious-lockeddown-symptom</metric>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <steppedValueSet variable="Taux-de-couverture-de-l'application-de-traçage" first="10" step="10" last="100"/>
    <enumeratedValueSet variable="Temps-d'attente-pour-la-réalisation-du-test">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Profondeur-temporelle-de-recherche-des-contacts">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-que-le-test-soit-efficace">
      <value value="0.7"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-de-respect-du-confinement">
      <value value="0.7"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Traçage et confinement systématique&quot;"/>
      <value value="&quot;Traçage et confinement sélectif&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V9_Scenario2-R0inf1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <metric>Max-Conf%</metric>
    <metric>total-population-locked%</metric>
    <metric>population-tested%</metric>
    <metric>total-nb-tests</metric>
    <metric>total-nb-contagious</metric>
    <metric>contagious-detected%</metric>
    <metric>contagious-lockeddown%</metric>
    <metric>proportion-non-contagious-lockeddown%</metric>
    <metric>proportion-total-contagious-lockeddown-tracked</metric>
    <metric>proportion-total-contagious-lockeddown-symptom</metric>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Temps-d'attente-pour-la-réalisation-du-test">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-que-le-test-soit-efficace">
      <value value="0.7"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-de-respect-du-confinement">
      <value value="0.7"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Confinement simple&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V9_Scenario1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Laisser-faire&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V9_Scenarios3-4-R0inf1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <metric>Max-Conf%</metric>
    <metric>total-population-locked%</metric>
    <metric>population-tested%</metric>
    <metric>total-nb-tests</metric>
    <metric>total-nb-contagious</metric>
    <metric>contagious-detected%</metric>
    <metric>contagious-lockeddown%</metric>
    <metric>proportion-non-contagious-lockeddown%</metric>
    <metric>proportion-total-contagious-lockeddown-tracked</metric>
    <metric>proportion-total-contagious-lockeddown-symptom</metric>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="0.5"/>
    </enumeratedValueSet>
    <steppedValueSet variable="Taux-de-couverture-de-l'application-de-traçage" first="10" step="10" last="100"/>
    <enumeratedValueSet variable="Temps-d'attente-pour-la-réalisation-du-test">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Profondeur-temporelle-de-recherche-des-contacts">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-que-le-test-soit-efficace">
      <value value="0.7"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-de-respect-du-confinement">
      <value value="0.7"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Traçage et confinement systématique&quot;"/>
      <value value="&quot;Traçage et confinement sélectif&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V9_Scenario1-R0inf1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Laisser-faire&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="0.5"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V10_Scenario1" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Laisser-faire&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="1"/>
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="0.5"/>
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="probability-asymptomatic-infection">
      <value value="0.1"/>
      <value value="0.3"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-seed?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V10_Scenario2" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <metric>Max-Conf%</metric>
    <metric>total-population-locked%</metric>
    <metric>population-tested%</metric>
    <metric>total-nb-tests</metric>
    <metric>total-nb-contagious</metric>
    <metric>contagious-detected%</metric>
    <metric>contagious-lockeddown%</metric>
    <metric>proportion-non-contagious-lockeddown%</metric>
    <metric>proportion-total-contagious-lockeddown-tracked</metric>
    <metric>proportion-total-contagious-lockeddown-symptom</metric>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="1"/>
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="0.5"/>
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="probability-asymptomatic-infection">
      <value value="0.1"/>
      <value value="0.3"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Temps-d'attente-pour-la-réalisation-du-test">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-que-le-test-soit-efficace">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-de-respect-du-confinement">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Confinement simple&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="Explo_V10_Scenarios3-4" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <exitCondition>not any? citizens with [contagious?]</exitCondition>
    <metric>MaxI%</metric>
    <metric>nb-non-S%</metric>
    <metric>epidemic-duration-final</metric>
    <metric>Max-Conf%</metric>
    <metric>total-population-locked%</metric>
    <metric>population-tested%</metric>
    <metric>total-nb-tests</metric>
    <metric>total-nb-contagious</metric>
    <metric>contagious-detected%</metric>
    <metric>contagious-lockeddown%</metric>
    <metric>proportion-non-contagious-lockeddown%</metric>
    <metric>proportion-total-contagious-lockeddown-tracked</metric>
    <metric>proportion-total-contagious-lockeddown-symptom</metric>
    <enumeratedValueSet variable="Nombre-de-cas-au-départ">
      <value value="1"/>
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0-fixé">
      <value value="0.5"/>
      <value value="1"/>
      <value value="2"/>
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="probability-asymptomatic-infection">
      <value value="0.1"/>
      <value value="0.3"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Temps-d'attente-pour-la-réalisation-du-test">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-que-le-test-soit-efficace">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Probabilité-de-respect-du-confinement">
      <value value="1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="Taux-de-couverture-de-l'application-de-traçage" first="10" step="10" last="100"/>
    <enumeratedValueSet variable="Temps-d'attente-pour-la-réalisation-du-test">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Profondeur-temporelle-de-recherche-des-contacts">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="SCENARIO">
      <value value="&quot;Traçage et confinement systématique&quot;"/>
      <value value="&quot;Traçage et confinement sélectif&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-seed?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

link-arn
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Polygon -7500403 true true 150 150 105 210 195 210
@#$#@#$#@
0
@#$#@#$#@
