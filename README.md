# Description  
Ce modèle CoVprehension s’intéresse aux applications mobiles de traçage épidémiologique et à leurs possibles effet sur l’épidémie de COVID-19. Il reprend les éléments de base du modèle de confinement de la Q6 en les enrichissant un peu.

Les journées sont découpées en quatre tranches de 6 heures. Les trois premières tranches sont occupées par des déplacements et des rencontres (en moyenne 10 par jours). La plupart des déplacements se font à proximité du domicile mais 20% d’entre eux se font à plus longue distance. La dernière tranche de la journée s’effectue au domicile, partagé par trois personnes en moyenne. Au total, 2000 personnes occupent ce petit territoire virtuel.

Par ailleurs, nous introduisons également une phase d’incubation (E pour Exposé) et deux catégories d’infection possibles : les symptomatiques et les asymptomatiques, ces dernières étant plus difficiles à identifier par simple diagnostic médical en raison de l’absence de symptômes. Il s’agit donc maintenant d’un modèle SEIR.

À l’initialisation, tous les individus sont sains. On injecte alors un petit nombre d’individus infectés (paramètre Nombre-de-cas-au-départ) dans cette population initiale et on simule la propagation du virus à partir des comportements des individus modélisés.

Les changements d’état s’opèrent de la manière suivante :  
Sain → Exposé : un individu sain deviendra exposé, au contact d’un individu infecté, selon une probabilité qui dépend de la valeur du R0 choisie et de la catégorie à laquelle appartient cet individu infecté (cf point suivant). La formule retenue pour calculer cette probabilité est la suivante : P(S→ E) = 1/R0 x c x d avec c le nombre de contacts moyens par jours (fixé à 10 environ) et d la durée de la période contagieuse.  
Exposé → Infecté : un individu restera dans l’état exposé pendant sa période d’incubation (fixée ici à 4 jours), au cours de laquelle il deviendra progressivement contagieux, jusqu’à devenir Infecté asymptomatique avec une probabilité de 0.3 et Infecté symptomatique avec une probabilité de 0.7 (ce qui correspond à une proportion d’infectés asymptomatiques dans la population de l’ordre de 30%). La contagiosité d’un individu symptomatique est estimée à partir du R0 (cf point précédent) et est considérée comme étant deux fois supérieure à celle d’un individu asymptomatique.  
Infecté → Guéri : au bout de 14 jours, un individu infecté est considéré comme guéri et non contagieux dans le modèle.

Sur cette base, quatre scénarios distincts sont proposés (paramètre SCENARIO), dans une perspective comparative :  
S1 : Laisser-faire : on ne fait rien, l’épidémie suit son cours sans aucune interférence  
S2 : Confinement simple : on identifie les porteurs symptomatiques (test) et on les confine avec leur famille  
S3 : Traçage et confinement systématique : les infecté symptomatiques sont systématiquement testés et ceux qui sont positifs sont confinés avec leur famille, tandis que leurs contacts (et leur famille) sont confinés sans être testés  
S4 : Traçage et confinement sélectif : els infecté symptomatiques sont systématiquement testés et ceux qui sont positifs sont confinés avec leur famille, tandis que leurs contacts (et leur famille) sont testés et confinés s’ils sont positifs, ainsi que leurs contacts et les contacts de leurs contacts…

# Marche à suivre  
Choisissez un scénario et fixez des conditions initiales (curseurs en bas à gauche de l’écran). Cliquez sur le bouton “Initialiser” puis lancez la simulation.

# Précisions sur le R0  
Le R0, ou nombre de reproduction de base, est un indicateur global de la dynamique épidémique. De manière intuitive on peut le voir comme un indicateur du nombre de personnes saines qu’une personne infectée infectera, en moyenne, pendant la durée de l’épidémie.  
Si R0 est inférieur à 1, l’épidémie va s’éteindre d’elle même, sans passer par une phase de flambée épidémique. Si R0 est supérieur à 1, alors l’épidémie pourra se développer.  
Vous pouvez tester ce point en fixant les paramètres suivants :  
- SCENARIO : “Laisser faire” - Nombre-de-cas-au-départ : 1 - R0-fixé : faites varier la valeur autour de 1, cliquez sur le bouton “Initialiser” puis lancez la simulation.

NB : vous pouvez éventuellement obtenir des départs d’épidémie pour des valeurs inférieures à 1, en raison du caractère discret et stochastique du modèle.  
Attention, cet indicateur est une composition de processus couplés : le taux de contacts dans la population, la probabilité de transmission du virus à chaque contact et la durée de la phase contagieuse de la maladie développée. Par ailleurs, comme tout indicateur global,il ne rend pas compte des situations locales, qui peuvent être très différenciées. Il doit donc être manipulé avec précaution.  
Son utilisation dans ce modèle permet de caractériser des dynamiques épidémiques types. A titre d’exemple, le R0 en France au moment de la mise en place de la période de confinement (17 mars 2020) était estimé à une valeur comprise entre 2,5 et 3. Après plusieurs semaines de confinement, il était estimé à 0,6.
# AUTEURS
Modèle développé par Arnaud Banos & Pierrick Tranouez pour CoVprehension (https://covprehension.org/)
